package me.duzhi.ilog.cms.hanlder;

import com.jfinal.handler.Handler;
import io.jpress.message.MessageKit;
import me.duzhi.ilog.cms.function.HtmlFilter;
import me.duzhi.ilog.cms.plugins.history.HistoryListener;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author ashang.peng@aliyun.com
 * @date 一月 07, 2017
 */

public class HtmlHandler extends Handler {
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("M/d");
    public static String before_date = null;

    @Override
    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
        if (!target.startsWith("/admin")) {
            try {
                HtmlFilter.setCompress(true);
                next.handle(target,request,response,isHandled);
            } finally {
                //一定要清楚
                HtmlFilter.setCompress(false);
            }
        }else{
            next.handle(target,request,response,isHandled);
        }
        String _date = SIMPLE_DATE_FORMAT.format(new Date());
        if(!_date.equals(before_date)){
            before_date=_date;
            MessageKit.sendMessage(HistoryListener.ACTION_LOAD_HISTORY);
        }
    }
}
